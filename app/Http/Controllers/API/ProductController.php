<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use Validator;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->has('limit') ? $request->limit : 15;
        $sortby = $request->sortby;
        $direction = $request->has('direction') ? $request->direction : 'asc';

        $products = Product::select('*');

        if (!empty($products)) {

            if ($request->has('search')) {
                $products = $products->where('title', 'like', '%' .$request->search . '%');
                $products = $products->orWhere('description', 'like', '%'. $request->search .'%');
                $products = $products->orWhere('detail', 'like', '%'. $request->search .'%');
            }

            if ($request->has('published')) {
                $products = $products->where('published', $request->published);
            }

            if($request->has('sortby')) {
                $products = $products->orderBy($sortby, $direction)->paginate($limit);
                $products->appends(['sortby' => $sortby, 'direction' => $direction])->links();
            } else {
                $products = $products->paginate($limit);
            }

            $products->appends(['limit' => $limit])->links();

            return ProductResource::collection($products);
        } else {
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'title' => 'required',
        ]);
   
        if($validator->fails()) {
            return $this->sendError('Doğrulama hatası', $validator->errors());       
        }
   
        $product = Product::create([
            'title' => $input['title'],
            'description' => !empty($input['description']) ? $input['description'] : NULL,
            'detail' => !empty($input['detail']) ? $input['detail'] : NULL,
            'photo' => !empty($input['photo']) ? $input['photo'] : NULL,
            'sort' => !empty($input['sort']) ? $input['sort'] : NULL,
            'published' => !empty($input['published']) ? $input['published'] : 0,
        ]);
   
        return $this->sendResponse(new ProductResource($product), 'Başarıyla oluşturuldu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
  
        if (is_null($product)) {
            return $this->sendError('Bulunamadı');
        }
   
        return $this->sendResponse(new ProductResource($product), 'Başarıyla alındı', 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
        ]);
   
        if($validator->fails()) {
            return $this->sendError('Doğrulama hatası', $validator->errors());       
        }
   
        $product->title = $input['title'];

        if(!empty($input['description'])) {
            $product->description = $input['description'];
        }

        if(!empty($input['detail'])) {
            $product->detail = $input['detail'];
        }

        if(!empty($input['photo'])) {
            $product->photo = $input['photo'];
        }

        if(!empty($input['sort'])) {
            $product->sort = $input['sort'];
        }
        
        if(!empty($input['published'])) {
            $product->published = $input['published'];
        }
        
        $product->save();
   
        return $this->sendResponse(new ProductResource($product), 'Başarıyla güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
   
        return $this->sendResponse([], 'Başarıyla silindi');
    }
}
