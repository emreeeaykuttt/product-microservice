<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'detail', 'photo', 'sort', 'published'];

    protected $appends = ['slug'];

    public function getSlugAttribute() 
    {
        return Str::slug($this->title);
    }
}
