<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'description' => $this->faker->sentence,
            'detail' => $this->faker->text,
            'sort' => $this->faker->unique()->numberBetween(1,25),
            'published' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
